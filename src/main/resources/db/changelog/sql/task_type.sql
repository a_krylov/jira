CREATE TABLE task_type
(
    task_type_id serial PRIMARY KEY,
	name varchar(50) not null
);

grant select on task_type to app;
grant usage on task_type_task_type_id_seq to app;
grant select, insert, update, delete on task_type to admin;

insert into task_type values (nextval('task_type_task_type_id_seq'), 'Feature');
insert into task_type values (nextval('task_type_task_type_id_seq'), 'Tech');
insert into task_type values (nextval('task_type_task_type_id_seq'), 'Bug');
