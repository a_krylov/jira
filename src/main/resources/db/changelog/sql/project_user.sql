CREATE TABLE project_user
(
    project_user_id serial PRIMARY KEY,
    system_user_id integer references system_user(user_id) not null,
	project_id integer references project(project_id) not null
);

ALTER TABLE project_user ADD CONSTRAINT constraint_project_user UNIQUE (system_user_id, project_id);

grant select on project_user to app;
grant usage on project_user_project_user_id_seq to app;
grant select, insert, update, delete on project_user to admin;

