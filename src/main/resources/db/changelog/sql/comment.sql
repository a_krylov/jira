CREATE TABLE comment
(
    comment_id serial PRIMARY KEY,
	task_id integer references task(task_id),
	author_id integer references project_user(project_user_id),
	text varchar(4000) not null,
	create_date timestamp not null
);

grant select, insert on comment to app;
grant usage on comment_comment_id_seq to app;
grant select, insert, update, delete on comment to admin;