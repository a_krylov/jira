package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum CommentQuery {
    GET_COMMENT_BY_TASK_ID("Получение comment по task_id",
            "select * from comment " +
                    "where task_id=:task_id "),

    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса",
            "select * from nextval('comment_comment_id_seq')"),

    INSERT_COMMENT("Создание comment",
            "insert into comment (" +
                    "comment_id, task_id, author_id, " +
                    "text, create_date) " +
                    "   values (:comment_id, :task_id, :author_id, " +
                    ":text, LOCALTIMESTAMP)");

    private final String description;
    private final String query;
}
