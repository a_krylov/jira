package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.TaskType;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import static ru.mephi.jira.dao.query.TaskTypeQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class TaskTypeDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public TaskTypeDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<TaskType> fetchAll() {
        return namedParameterJdbcTemplate.query(GET_TASK_TYPES.getQuery(), new TaskTypeRowMapper());
    }

    public TaskType fetchById(Integer taskTypeId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_type_id", taskTypeId);
        return namedParameterJdbcTemplate.queryForObject(GET_TASK_TYPE_BY_ID.getQuery(), params, new TaskTypeRowMapper());
    }

    public Integer insertTaskType(TaskType taskType) {
        Integer taskTypeId = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_type_id", taskTypeId)
                .addValue("name", taskType.getName());
        namedParameterJdbcTemplate.update(INSERT_TASK_TYPE.getQuery(), params);
        return taskTypeId;
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }

    public static class TaskTypeRowMapper implements RowMapper<TaskType> {
        @Override
        public TaskType mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new TaskType(
                    rs.getInt("task_type_id"),
                    rs.getString("name")
            );
        }
    }
}
