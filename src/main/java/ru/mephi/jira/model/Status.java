package ru.mephi.jira.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Status {
    private Integer id;
    private String name;

    public Status(Integer id) {
        this.id = id;
    }
}
