package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.*;
import ru.mephi.jira.model.forController.CreateTaskRequest;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import static ru.mephi.jira.dao.query.TaskQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class TaskDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public TaskDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Integer> fetchByProjectId(Integer projectId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("project_id", projectId);
        return namedParameterJdbcTemplate.query(GET_TASK_ID_BY_PROJECT_ID.getQuery(), params, (rs, i) -> rs.getInt("task_id"));
    }

    public Task fetchById(Integer taskId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_id", taskId);
        return namedParameterJdbcTemplate.queryForObject(GET_TASK_BY_ID.getQuery(), params, (rs, i) -> {
            Task task = Task.builder()
                    .id(rs.getInt("task_id"))
                    .project(new Project(rs.getInt("project_id")))
                    .name(rs.getString("task_name"))
                    .shortDescription(rs.getString("short_description"))
                    .fullDescription(rs.getString("full_description") != null ? rs.getString("full_description") : null)
                    .status(new Status(rs.getInt("status_id")))
                    .taskType(new TaskType(rs.getInt("task_type_id")))
                    .author(new ProjectUser(rs.getInt("author_id")))
                    .assignee(new ProjectUser(rs.getInt("assignee_id")))
                    .assignee(rs.getInt("assignee_id") != 0 ? new ProjectUser(rs.getInt("assignee_id")) : null)
                    .createDate(rs.getTimestamp("create_date").toLocalDateTime())
                    .planDate(rs.getDate("plan_date") != null ? rs.getDate("plan_date").toLocalDate() : null)
                    .endDate(rs.getDate("end_date") != null ? rs.getDate("end_date").toLocalDate() : null)
                    .priority(rs.getInt("priority"))
                    .build();
            return task;
        });

    }

    public void updateFullDescription(Integer taskId, String fullDescription) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_id", taskId)
                .addValue("full_description", fullDescription);
        namedParameterJdbcTemplate.update(UPDATE_FULL_DESCRIPTION_TO_TASK.getQuery(), params);
    }

    public void updatePlanDate(Integer taskId, LocalDate planDate) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_id", taskId)
                .addValue("plan_date", Date.valueOf(planDate));
        namedParameterJdbcTemplate.update(UPDATE_PLAN_DATE_TO_TASK.getQuery(), params);
    }

    public void updateEndDate(Integer taskId, LocalDate endDate) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_id", taskId)
                .addValue("end_date", Date.valueOf(endDate));
        namedParameterJdbcTemplate.update(UPDATE_END_DATE_TO_TASK.getQuery(), params);
    }

    public void updateStatus(Integer taskId, Integer statusId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_id", taskId)
                .addValue("status_id", statusId);
        namedParameterJdbcTemplate.update(UPDATE_STATUS_TO_TASK.getQuery(), params);
    }

    public void updateAssignee(Integer taskId, Integer assigneeId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_id", taskId)
                .addValue("assignee_id", assigneeId);
        namedParameterJdbcTemplate.update(UPDATE_ASSIGNEE_ID_TO_TASK.getQuery(), params);
    }

    public Integer insertTask(CreateTaskRequest createTaskRequest) {
        Integer taskId = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_id", taskId)
                .addValue("project_id", createTaskRequest.getProjectId())
                .addValue("task_name", createTaskRequest.getName())
                .addValue("short_description", createTaskRequest.getShortDescription())
                .addValue("full_description", createTaskRequest.getFullDescription() != null ? createTaskRequest.getFullDescription() : null)
                .addValue("task_type_id", createTaskRequest.getTaskTypeId())
                .addValue("author_id", createTaskRequest.getAuthorIdProjectUser())
                .addValue("plan_date", createTaskRequest.getPlanDate() != null ? createTaskRequest.getPlanDate() : null)
                .addValue("priority", createTaskRequest.getPriority());
        namedParameterJdbcTemplate.update(INSERT_TASK.getQuery(), params);
        return taskId;
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }

}
