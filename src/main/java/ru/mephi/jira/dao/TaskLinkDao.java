package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.Comment;
import ru.mephi.jira.model.LinkedTask;
import ru.mephi.jira.model.ProjectUser;
import ru.mephi.jira.model.Task;
import ru.mephi.jira.model.forController.CreateCommentRequest;
import ru.mephi.jira.model.forController.CreateTaskLinkRequest;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.mephi.jira.dao.query.CommentQuery.GET_COMMENT_BY_TASK_ID;
import static ru.mephi.jira.dao.query.TaskLinksQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class TaskLinkDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public TaskLinkDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<LinkedTask> fetchById(Integer taskId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("main_task_id", taskId);
        return namedParameterJdbcTemplate.query(GET_TASK_LINKS_BY_TASK_ID.getQuery(), params, (rs, i) -> {
            LinkedTask linkedTask = LinkedTask.builder()
                    .linkedTask(new Task(rs.getInt("slave_task_id")))
                    .linkType(rs.getString("link_type"))
                    .build();
            return linkedTask;
        });
    }

    public Integer insertTaskLink(Integer taskId, CreateTaskLinkRequest createTaskLinkRequest) {
        Integer taskLinkId = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", taskLinkId)
                .addValue("main_task_id", taskId)
                .addValue("slave_task_id", createTaskLinkRequest.getSlaveTaskId())
                .addValue("link_type", createTaskLinkRequest.getLinkType());
        namedParameterJdbcTemplate.update(INSERT_TASK_LINK.getQuery(), params);
        return taskLinkId;
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }

}
