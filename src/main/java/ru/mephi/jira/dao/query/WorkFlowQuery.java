package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum WorkFlowQuery {
    GET_STATUSES("Получение всех статусов",
            "select * from status order by status_id"),

    GET_ALL_CURRENT_STATUS_FOR_TASK_TYPE("Получение всех переходов для типа задачи",
            "select distinct s1.* from workflow w " +
                    "join status s1 on s1.status_id=w.current_status_id " +
                    "where w.task_type_id=:task_type_id " +
                    "order by s1.status_id"),

    GET_ALLOWED_TRANSITIONS_FOR_STATUS("Получение переходов для конкретного статуса",
            "select s2.* from workflow w " +
                    "join status s2 on s2.status_id=w.allowed_status_id " +
                    "where w.task_type_id=:task_type_id " +
                    "and w.current_status_id=:current_status_id " +
                    "order by w.id"),

    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса wokrflow",
            "select * from nextval('workflow_id_seq')"),

    INSERT_WORKFLOW_RAW("Создание строчки в воркфлоу",
            "insert into workflow (id, current_status_id, allowed_status_id, task_type_id) " +
                    "values (:id, :current_status_id, :allowed_status_id, :task_type_id)");

    private final String description;
    private final String query;
}
