package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ProjectQuery {
    GET_PROJECTS("Получение всех systemUser",
            "select * from project order by project_id"),

    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса",
            "select * from nextval('project_project_id_seq')"),

    INSERT_PROJECT("Создание project",
            "insert into project (project_id, project_name, short_description, create_date) " +
                    "values (:project_id, :project_name, :short_description, LOCALTIMESTAMP)"),

    UPDATE_OWNER_TO_PROJECT("Обновление owner_id проекта",
            "update project set owner_id = :owner_id " +
                    "where project_id=:project_id"),

    UPDATE_FULL_DESCRIPTION_TO_PROJECT("Обновление full_description проекта",
            "update project set full_description = :full_description " +
                    "where project_id=:project_id");

    private final String description;
    private final String query;
}
