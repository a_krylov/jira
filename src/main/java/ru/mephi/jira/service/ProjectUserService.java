package ru.mephi.jira.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mephi.jira.dao.*;
import ru.mephi.jira.model.ProjectRole;
import ru.mephi.jira.model.ProjectUser;
import ru.mephi.jira.model.SystemUser;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectUserService {
    private final ProjectDao projectDao;
    private final SystemUserDao systemUserDao;
    private final ProjectUserDao projectUserDao;
    private final ProjectUserRoleDao projectUserRoleDao;
    private final ProjectRoleDao projectRoleDao;

    public List<ProjectUser> getProjectUsers(Integer projectId) {
        List<ProjectUser> projectUsers = projectUserDao.getByProjectId(projectId);
        projectUsers.forEach(projectUser -> fullProjectUser(projectUser));
        return projectUsers;
    }

    public ProjectUser getProjectUserById(Integer projectUserId) {
        ProjectUser projectUser = projectUserDao.getByProjectUserId(projectUserId);
        return fullProjectUser(projectUser);
    }

    private ProjectUser fullProjectUser(ProjectUser projectUser){
        SystemUser systemUser = systemUserDao.fetchByUserId(projectUser.getSystemUser().getId());
        projectUser.setSystemUser(systemUser);

        List<ProjectRole> projectRoles = projectUserRoleDao.getByProjectUserId(projectUser.getId());
        projectUser.setProjectRoles(projectRoles);
        return projectUser;
    }

}
