package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.Status;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;

import static ru.mephi.jira.dao.query.WorkFlowQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class WorkflowDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public WorkflowDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Status> getAllStatuses() {
        return namedParameterJdbcTemplate.query(GET_STATUSES.getQuery(), (rs, i) ->
                new Status(rs.getInt("status_id"),
                        rs.getString("name"))
        );
    }

    public List<Status> getAllAllowedStatusesForStatus(Integer currentStatusId, Integer taskTypeId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_type_id", taskTypeId)
                .addValue("current_status_id", currentStatusId);
        return namedParameterJdbcTemplate.query(GET_ALLOWED_TRANSITIONS_FOR_STATUS.getQuery(), params, (rs, i) ->
                new Status(rs.getInt("status_id"),
                        rs.getString("name"))
        );
    }

    public List<Status> getAllCurrentStatusesForTaskType(Integer taskTypeId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_type_id", taskTypeId);
        return namedParameterJdbcTemplate.query(GET_ALL_CURRENT_STATUS_FOR_TASK_TYPE.getQuery(), params, (rs, i) ->
                new Status(rs.getInt("status_id"),
                        rs.getString("name"))
        );
    }

    public void insertWorkflowRaw(Integer currentStatusId, Integer allowedStatusId, Integer taskTypeId) {
        Integer workflowRawId = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", workflowRawId)
                .addValue("current_status_id", currentStatusId)
                .addValue("allowed_status_id", allowedStatusId)
                .addValue("task_type_id", taskTypeId);
        namedParameterJdbcTemplate.update(INSERT_WORKFLOW_RAW.getQuery(), params);
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }
}
