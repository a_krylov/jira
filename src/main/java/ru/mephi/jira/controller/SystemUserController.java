package ru.mephi.jira.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.mephi.jira.dao.SystemUserDao;
import ru.mephi.jira.model.SystemUser;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class SystemUserController {
    private final SystemUserDao systemUserDao;

    @GetMapping(value = "/systemUser/get", produces = APPLICATION_JSON_VALUE)
    public List<SystemUser> getSystemUsers() {
        return systemUserDao.fetchAll();
    }

    @PostMapping(value = "/systemUser/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public SystemUser createSystemUser(@RequestBody SystemUser systemUser) throws DataAccessException {
        Integer userId = systemUserDao.insertSystemUser(systemUser);
        systemUser.setId(userId);
        return systemUser;
    }
}
