CREATE TABLE project_user_role
(
    id serial PRIMARY KEY,
    project_user_id integer references project_user(project_user_id) not null,
	project_role_id integer references project_role(project_role_id) not null
);

grant select on project_user_role to app;
grant usage on project_user_role_id_seq to app;
grant select, insert, update, delete on project_user_role to admin;

