package ru.mephi.jira.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class Comment {
    private Integer id;
    private ProjectUser projectUserAuthor;
    private LocalDateTime createDate;
    private String text;
}
