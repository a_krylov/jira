CREATE TABLE project
(
    project_id serial PRIMARY KEY,
	project_name varchar(50) not null,
	short_description varchar(50) not null,
	full_description varchar(1000),
	create_date timestamp not null,
	owner_id integer references system_user(user_id)
);

grant select, insert on project to app;
grant usage on project_project_id_seq to app;
grant select, insert, update, delete on project to admin;