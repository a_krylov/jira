package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.*;
import ru.mephi.jira.model.forController.CreateCommentRequest;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;

import static ru.mephi.jira.dao.query.CommentQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class CommentDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public CommentDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<Comment> fetchById(Integer taskId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_id", taskId);
        return namedParameterJdbcTemplate.query(GET_COMMENT_BY_TASK_ID.getQuery(), params, (rs, i) -> {
            Comment comment = Comment.builder()
                    .id(rs.getInt("comment_id"))
                    .projectUserAuthor(new ProjectUser(rs.getInt("author_id")))
                    .text(rs.getString("text"))
                    .createDate(rs.getTimestamp("create_date").toLocalDateTime())
                    .build();
            return comment;
        });
    }

    public Integer insertComment(Integer taskId, CreateCommentRequest createCommentRequest) {
        Integer commentId = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("comment_id", commentId)
                .addValue("task_id", taskId)
                .addValue("author_id", createCommentRequest.getProjectUserIdAuthor())
                .addValue("text", createCommentRequest.getText());
        namedParameterJdbcTemplate.update(INSERT_COMMENT.getQuery(), params);
        return commentId;
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }

}
