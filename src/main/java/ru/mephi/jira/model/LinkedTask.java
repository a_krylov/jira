package ru.mephi.jira.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class LinkedTask {
    private Task linkedTask;
    private String linkType;
}
