package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.ProjectRole;
import ru.mephi.jira.model.ProjectUser;
import ru.mephi.jira.model.SystemUser;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import static ru.mephi.jira.dao.query.ProjectUserQuery.GET_PROJECT_USER_BY_PROJECT_ID;
import static ru.mephi.jira.dao.query.ProjectUserRoleQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class ProjectUserRoleDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public ProjectUserRoleDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<ProjectRole> getByProjectUserId(Integer projectUserId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("project_user_id", projectUserId);
        return namedParameterJdbcTemplate.query(GET_PROJECT_ROLE_BY_PROJECT_USER_ID.getQuery(), params,
                (rs, i) -> new ProjectRole(
                        rs.getInt("project_role_id"),
                        rs.getString("project_role"),
                        rs.getString("description")
                ));
    }

    public Integer insertProjectUserRole(Integer projectUserId, Integer projectRoleId) {
        Integer id = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("id", id)
                .addValue("project_user_id", projectUserId)
                .addValue("project_role_id", projectRoleId);
        namedParameterJdbcTemplate.update(INSERT_PROJECT_USER_ROLE.getQuery(), params);
        return id;
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }

}
