CREATE TABLE task_links
(
    id serial PRIMARY KEY,
	main_task_id integer not null references task(task_id),
	slave_task_id integer not null references task(task_id),
	link_type varchar(20) not null
);

grant select, insert on task_links to app;
grant usage on task_links_id_seq to app;
grant select, insert, update, delete on task_links to admin;