CREATE TABLE workflow
(
    id serial PRIMARY KEY,
	current_status_id integer not null references status(status_id),
	allowed_status_id integer not null references status(status_id),
	task_type_id integer not null references task_type(task_type_id),
	UNIQUE (current_status_id, allowed_status_id, task_type_id)
);

grant select on workflow to app;
grant usage on workflow_id_seq to app;
grant select, insert, update, delete on workflow to admin;

insert into workflow values (nextval('workflow_id_seq'), 1, 3, 1);
insert into workflow values (nextval('workflow_id_seq'), 3, 4, 1);
insert into workflow values (nextval('workflow_id_seq'), 4, 6, 1);
insert into workflow values (nextval('workflow_id_seq'), 6, 7, 1);
insert into workflow values (nextval('workflow_id_seq'), 7, 8, 1);
insert into workflow values (nextval('workflow_id_seq'), 8, 11, 1);
insert into workflow values (nextval('workflow_id_seq'), 11, 10, 1);
insert into workflow values (nextval('workflow_id_seq'), 10, 12, 1);
insert into workflow values (nextval('workflow_id_seq'), 12, 14, 1);
insert into workflow values (nextval('workflow_id_seq'), 14, 15, 1);
insert into workflow values (nextval('workflow_id_seq'), 1, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 3, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 4, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 6, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 7, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 8, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 11, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 10, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 12, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 14, 16, 1);
insert into workflow values (nextval('workflow_id_seq'), 12, 6, 1);