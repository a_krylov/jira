package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum ProjectUserQuery {
    GET_PROJECT_USER_BY_PROJECT_ID("Получение projectUser по project_id",
            "select * from project_user where project_id=:project_id"),

    GET_PROJECT_USER_BY_PROJECT_USER_ID("Получение projectUser по project_user_id",
            "select * from project_user where project_user_id=:project_user_id"),


    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса",
            "select * from nextval('project_user_project_user_id_seq')"),

    INSERT_PROJECT_USER("Создание projectUser",
            "insert into project_user (project_user_id, system_user_id, project_id) values (:project_user_id, :system_user_id, :project_id)");

    private final String description;
    private final String query;
}
