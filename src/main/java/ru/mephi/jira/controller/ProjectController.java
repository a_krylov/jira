package ru.mephi.jira.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;
import ru.mephi.jira.dao.ProjectDao;
import ru.mephi.jira.dao.ProjectUserDao;
import ru.mephi.jira.exeption.NotEnoughRightException;
import ru.mephi.jira.model.Project;
import ru.mephi.jira.model.forController.ProjectUserCreationResponse;
import ru.mephi.jira.model.forController.SetFullDescriptionRequest;
import ru.mephi.jira.service.ProjectService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectController {
    private final ProjectDao projectDao;
    private final ProjectUserDao projectUserDao;
    private final ProjectService projectService;

    @GetMapping(value = "/project/get", produces = APPLICATION_JSON_VALUE)
    public List<Project> getProjects() {
        return projectService.getAllProjects();
    }

    //Создавать надо без овнера
    @PostMapping(value = "/project/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public Project createProject(@RequestBody Project project) throws DataAccessException {
        Integer userId = projectDao.insertProject(project);
        project.setId(userId);
        return project;
    }

    @PostMapping(value = "/project/setOwner/{projectId}", produces = APPLICATION_JSON_VALUE)
    public void setOwnerToProject(@PathVariable("projectId") Integer projectId, @RequestParam Integer ownerId) throws NotEnoughRightException, DataAccessException {
        projectService.updateOwnerId(projectId, ownerId);
    }

    @PostMapping(value = "/project/setFullDescription/{projectId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void setFullDescriptionToProject(@PathVariable("projectId") Integer projectId, @RequestBody SetFullDescriptionRequest setFullDescriptionRequest) throws DataAccessException {
        projectDao.updateFullDescription(projectId, setFullDescriptionRequest.getFullDescription());
    }

    @PostMapping(value = "/project/setProjectUser/{projectId}", produces = APPLICATION_JSON_VALUE)
    @ResponseBody
    public ProjectUserCreationResponse setProjectUserToProject(@PathVariable("projectId") Integer projectId, @RequestParam Integer systemUserId) throws DataAccessException {
        ProjectUserCreationResponse response = new ProjectUserCreationResponse(projectUserDao.insertProjectUser(systemUserId, projectId));
        return response;
    }

}
