package ru.mephi.jira.model.forController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CreateTaskRequest {
    private Integer projectId;
    private String name;
    private String shortDescription;
    private String fullDescription;
    private Integer taskTypeId;
    private Integer priority;
    private Integer authorIdProjectUser;
    private LocalDate planDate;
}
