package ru.mephi.jira.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mephi.jira.dao.*;
import ru.mephi.jira.exeption.NotStatusException;
import ru.mephi.jira.model.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TaskService {
    private final TaskDao taskDao;
    private final TaskTypeDao taskTypeDao;
    private final CommentDao commentDao;
    private final FileDao fileDao;
    private final TaskLinkDao taskLinkDao;
    private final WorkflowService workflowService;
    private final ProjectService projectService;
    private final ProjectUserService projectUserService;

    public void updateStatus(Integer taskId, Integer statusId) throws NotStatusException {
        Task task = getFullTask(taskId);
        List<Status> allowedStatuses = workflowService.getAllowedStatusesForCurrentStatus(
                        task.getStatus().getId(),
                        task.getTaskType().getId())
                .getAllowedStatuses();
        allowedStatuses.stream().filter(status1 -> status1.getId().equals(statusId))
                .findFirst()
                .orElseThrow(() -> new NotStatusException());
        taskDao.updateStatus(taskId, statusId);
    }

    public void updateAssignee(Integer taskId, Integer projectUserId) {
        //toDo: тут должны быть валидации, можно ли назначить на этого юзера
        taskDao.updateAssignee(taskId, projectUserId);
    }

    public List<Task> getFullTasksByProjectId(Integer projectId) {
        List<Task> tasks = new ArrayList<>();
        List<Integer> taskIds = taskDao.fetchByProjectId(projectId);
        taskIds.forEach(taskId -> tasks.add(getFullTask(taskId)));
        return tasks;
    }

    public Task getFullTask(Integer taskId) {
        Task task = getLightTask(taskId);
        task.setProject(projectService.getProjectById(task.getProject().getId()));
        task.setStatus(workflowService.getStatusById(task.getStatus().getId()));
        task.setTaskType(taskTypeDao.fetchById(task.getTaskType().getId()));
        task.setAuthor(projectUserService.getProjectUserById(task.getAuthor().getId()));
        if (task.getAssignee() != null)
            task.setAssignee(projectUserService.getProjectUserById(task.getAssignee().getId()));
        task.setComments(getComments(taskId));
        task.setFiles(fileDao.fetchById(taskId));
        task.setLinkedTasks(getTaskLinks(taskId));

        //toDO: добавить требования)))
        return task;
    }

    private Task getLightTask(Integer taskId) {
        return taskDao.fetchById(taskId);
    }

    private List<Comment> getComments(Integer taskId) {
        List<Comment> comments = commentDao.fetchById(taskId);
        comments.forEach(comment -> comment.setProjectUserAuthor(
                projectUserService.getProjectUserById(
                        comment.getProjectUserAuthor().getId())));
        return comments;
    }

    private List<LinkedTask> getTaskLinks(Integer taskId) {
        List<LinkedTask> taskLinks = taskLinkDao.fetchById(taskId);
        taskLinks.forEach(linkedTask -> linkedTask.setLinkedTask(getLightTask(linkedTask.getLinkedTask().getId())));

        return taskLinks;
    }
}
