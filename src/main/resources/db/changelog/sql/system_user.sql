CREATE TABLE system_user
(
    user_id serial PRIMARY KEY,
	name varchar(100) not null,
	login varchar(20) not null,
	system_role varchar(30) references system_role(system_role)
);

grant select, insert on system_user to app;
grant usage on system_user_user_id_seq to app;
grant select, insert, update, delete on system_user to admin;

insert into system_user (name, login, system_role) values ('Иванов Иван', 'i.ivanov', 'user');
