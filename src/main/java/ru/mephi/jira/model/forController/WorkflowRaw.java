package ru.mephi.jira.model.forController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import ru.mephi.jira.model.Status;

import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class WorkflowRaw {
    private Integer currentStatusId;
    private Integer allowedStatusId;
    private Integer taskTypeId;
}
