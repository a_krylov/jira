package ru.mephi.jira.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class SystemUser {
    private Integer id;
    private String name;
    private String login;
    private String systemRole;

    public SystemUser(String name, String login, String systemRole) {
        this.name = name;
        this.login = login;
        this.systemRole = systemRole;
    }

    public SystemUser(int id) {
        this.id = id;
    }
}
