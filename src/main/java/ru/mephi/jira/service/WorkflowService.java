package ru.mephi.jira.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;
import ru.mephi.jira.dao.TaskTypeDao;
import ru.mephi.jira.dao.WorkflowDao;
import ru.mephi.jira.model.Status;
import ru.mephi.jira.model.TaskType;
import ru.mephi.jira.model.Workflow;
import ru.mephi.jira.model.forController.WorkflowRaw;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WorkflowService {
    private final WorkflowDao workflowDao;
    private final TaskTypeDao taskTypeDao;

    public void insertWorkflowRaw(WorkflowRaw workflowRaw) {
        workflowDao.insertWorkflowRaw(
                workflowRaw.getCurrentStatusId(),
                workflowRaw.getAllowedStatusId(),
                workflowRaw.getTaskTypeId());
    }

    public Workflow getAllowedStatusesForCurrentStatus(Integer currentStatusId, Integer taskTypeId) {
        Status currentStatus = getStatusById(currentStatusId);
        List<Status> statuses = workflowDao.getAllAllowedStatusesForStatus(
                currentStatusId,
                taskTypeId);
        return new Workflow(currentStatus, statuses);
    }

    public List<Workflow> getAllowedStatusesForTaskType(Integer taskTypeId) {
        List<Workflow> workflows = new ArrayList<>();
        List<Status> currentStatuses = workflowDao.getAllCurrentStatusesForTaskType(taskTypeId);

        currentStatuses.forEach(currentStatus -> {
            List<Status> allowedStatuses = workflowDao.getAllAllowedStatusesForStatus(
                    currentStatus.getId(),
                    taskTypeId);
            Workflow workflow = new Workflow(currentStatus, allowedStatuses);
            workflows.add(workflow);
        });
        return workflows;
    }

    //toDo: сделать нормальный Exception
    public Status getStatusById(Integer inStatus) {
        return workflowDao.getAllStatuses().stream()
                .filter(status -> status.getId().equals(inStatus))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Not Found Status - " + inStatus));
    }

    public TaskType getTaskTypeById(Integer taskTypeId) {
        return taskTypeDao.fetchAll().stream()
                .filter(taskType -> taskType.getId().equals(taskTypeId))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Not Found taskType - " + taskTypeId));
    }
}
