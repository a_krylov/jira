# JIRA

## Открыть наше приложение в браузере
Открыть в браузере: **http://localhost:8080/swagger-ui/index.html**

## Работа с докером Postgress и PgAdmin. 
Используется **docker-compose** проверьте его наличие.

###	В папке:
  Возможно надо добавить доп права на эти папки, иначе может не запуститься
  - Файлы с БД: **PostgressData\Source\Data**
  - Cкрипты инициализации базы (будет вызываться в момент ее создания, только создания): **PostgressData\Source\Init Database**
  - Хранятся настройки PgAdmin: **PostgressData\Source\pgadmin** 
  - Скрипты из JAVA (не пригодились, инициализация их в самом приложениии): **PostgressData\Init** 

### Чтобы запустить 
  - Находится в корневой директории
  - Выполнить команду: **docker-compose -f "doker-compose.yml" up -d**
  
### Чтобы остановить
  - Находится в корневой директории
  - Выполнить команду: **docker-compose -f "doker-compose.yml" down**

### Чтобы посмотреть запущенные контейнеры
  - Выполнить команду: **docker ps**
  - Образ для Postgress называется: **postgres:14** 
  - Имя для Postgress называется: **postgres_container** 
  - Образ для PgAdmin называется: **dpage/pgadmin4:latest**
  - Имя для PgAdmin называется: **pgadmin_container**

### Чтобы посмотреть логи контейнеров
  - Выполнить команду: **docker ps**
  - Найти нужный образ и скопировать его  *CONTAINER ID*
  - Выполнить команду: **docker logs *CONTAINER_ID***

### Чтобы запусть PgAdmin в браузере
  - Открыть в браузере: **http://localhost:5050/browser/**
  - Запускается примерно минуту
  - Пользователь: **app**
  - Пароль: **app**