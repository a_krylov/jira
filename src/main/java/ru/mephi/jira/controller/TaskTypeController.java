package ru.mephi.jira.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.mephi.jira.dao.TaskTypeDao;
import ru.mephi.jira.model.TaskType;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TaskTypeController {
    private final TaskTypeDao taskTypeDao;

    @GetMapping(value = "/taskType/get", produces = APPLICATION_JSON_VALUE)
    public List<TaskType> getProjects() {
        return taskTypeDao.fetchAll();
    }

    //Создавать надо без айди
    @PostMapping(value = "/taskType/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public TaskType createProject(@RequestBody TaskType taskType) throws DataAccessException {
        Integer taskTypeId = taskTypeDao.insertTaskType(taskType);
        taskType.setId(taskTypeId);
        return taskType;
    }

}
