CREATE TABLE status
(
    status_id serial PRIMARY KEY,
	name varchar(50) not null
);

grant select on status to app;
grant usage on status_status_id_seq to app;
grant select, insert, update, delete on status to admin;

insert into status values (nextval('status_status_id_seq'), 'New');
insert into status values (nextval('status_status_id_seq'), 'Assigned');
insert into status values (nextval('status_status_id_seq'), 'Ready to specification');
insert into status values (nextval('status_status_id_seq'), 'Technical specification');
insert into status values (nextval('status_status_id_seq'), 'Business specification');
insert into status values (nextval('status_status_id_seq'), 'Ready to develop');
insert into status values (nextval('status_status_id_seq'), 'Developing');
insert into status values (nextval('status_status_id_seq'), 'Ready to review');
insert into status values (nextval('status_status_id_seq'), 'Ready to install');
insert into status values (nextval('status_status_id_seq'), 'Ready to test');
insert into status values (nextval('status_status_id_seq'), 'Reviewing');
insert into status values (nextval('status_status_id_seq'), 'Testing');
insert into status values (nextval('status_status_id_seq'), 'Regression testing');
insert into status values (nextval('status_status_id_seq'), 'Ready to release');
insert into status values (nextval('status_status_id_seq'), 'Done');
insert into status values (nextval('status_status_id_seq'), 'Cancelled');
