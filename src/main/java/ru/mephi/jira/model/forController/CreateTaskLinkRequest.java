package ru.mephi.jira.model.forController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class CreateTaskLinkRequest {
    private Integer slaveTaskId;
    private String linkType;
}
