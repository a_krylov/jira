package ru.mephi.jira.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.mephi.jira.dao.*;
import ru.mephi.jira.exeption.NotEnoughRightException;
import ru.mephi.jira.model.Project;
import ru.mephi.jira.model.ProjectUser;
import ru.mephi.jira.model.SystemUser;

import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectService {
    private final ProjectDao projectDao;
    private final SystemUserDao systemUserDao;
    private final ProjectUserDao projectUserDao;
    private final ProjectUserRoleDao projectUserRoleDao;
    private final ProjectRoleDao projectRoleDao;

    private final static String systemUserRole = "user";
    private final static String projectUserRole = "owner";

    //toDO:по-хорошему надо еще проверку, есть ли projectUser уже такой и удалять старого овнера из projectUserRole
    @Transactional
    public void updateOwnerId(Integer projectId, Integer ownerId) throws NotEnoughRightException {
        SystemUser systemUser = systemUserDao.fetchByUserId(ownerId);
        if (systemUser.getSystemRole().equals(systemUserRole))
            projectDao.updateOwnerId(projectId, ownerId);
        else
            throw new NotEnoughRightException();
        ProjectUser projectUser = projectUserDao.getByProjectId(projectId).stream()
                .filter(projectUser1 -> projectUser1.getSystemUser().getId() == ownerId)
                .findFirst()
                .orElse(null);
        Integer projectUserId = projectUser != null ? projectUser.getId() : null;
        if (projectUserId == null)
            projectUserId = projectUserDao.insertProjectUser(ownerId, projectId);
        Integer projectRoleId = projectRoleDao.fetchByName(projectUserRole);
        projectUserRoleDao.insertProjectUserRole(projectUserId, projectRoleId);
    }

    public List<Project> getAllProjects() {
        List<Project> projects = projectDao.fetchAll();
        projects.forEach(project -> {
            if (project.getOwner() != null) {
                SystemUser owner = systemUserDao.fetchByUserId(project.getOwner().getId());
                project.setOwner(owner);
            }
        });
        return projects;
    }

    public Project getProjectById(Integer projectId) {
        List<Project> projects = getAllProjects();
        return projects.stream()
                .filter(project -> project.getId().equals(projectId))
                .findFirst()
                .orElse(null);
    }

}
