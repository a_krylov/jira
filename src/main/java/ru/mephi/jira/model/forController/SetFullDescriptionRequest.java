package ru.mephi.jira.model.forController;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class SetFullDescriptionRequest {
    private String fullDescription;
}
