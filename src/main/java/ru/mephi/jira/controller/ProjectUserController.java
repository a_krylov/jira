package ru.mephi.jira.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;
import ru.mephi.jira.dao.ProjectRoleDao;
import ru.mephi.jira.dao.ProjectUserDao;
import ru.mephi.jira.dao.ProjectUserRoleDao;
import ru.mephi.jira.model.Project;
import ru.mephi.jira.model.ProjectRole;
import ru.mephi.jira.model.ProjectUser;
import ru.mephi.jira.model.SystemUser;
import ru.mephi.jira.service.ProjectService;
import ru.mephi.jira.service.ProjectUserService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ProjectUserController {
    private final ProjectUserRoleDao projectUserRoleDao;
    private final ProjectUserService projectUserService;

    @GetMapping(value = "/projectUser/getAllByProject", produces = APPLICATION_JSON_VALUE)
    public List<ProjectUser> getProjectUsers(@RequestParam Integer projectId) {
        return projectUserService.getProjectUsers(projectId);
    }


    @PostMapping(value = "/projectUser/setRole/{projectUserId}", produces = APPLICATION_JSON_VALUE)
    public void setProjectUserToProject(@PathVariable("projectUserId") Integer projectUserId, @RequestParam Integer projectRoleId) throws DataAccessException {
        projectUserRoleDao.insertProjectUserRole(projectUserId, projectRoleId);
    }

}
