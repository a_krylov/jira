package ru.mephi.jira.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class File {
    private Integer id;
    private String name;
    private String path;
}
