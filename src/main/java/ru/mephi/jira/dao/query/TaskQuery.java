package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TaskQuery {
    GET_TASK_BY_ID("Получение task",
            "select * from task " +
                    "where task_id=:task_id "),

    GET_TASK_ID_BY_PROJECT_ID("Получение taskId по projectId",
            "select task_id from task " +
                    "where project_id=:project_id "),


    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса",
            "select * from nextval('task_task_id_seq')"),

    INSERT_TASK("Создание task",
            "insert into task (" +
                    "task_id, project_id, task_name, " +
                    "short_description, full_description, status_id, " +
                    "task_type_id, author_id, create_date, " +
                    "plan_date, priority) " +
                    "   values (:task_id, :project_id, :task_name, " +
                    ":short_description, :full_description, 1, " +
                    ":task_type_id, :author_id, LOCALTIMESTAMP, " +
                    ":plan_date, :priority)"),

    UPDATE_FULL_DESCRIPTION_TO_TASK("Обновление full_description задачи",
            "update task set full_description = :full_description " +
                    "where task_id=:task_id"),

    UPDATE_PLAN_DATE_TO_TASK("Обновление plan_date задачи",
            "update task set plan_date = :plan_date " +
                    "where task_id=:task_id"),

    UPDATE_END_DATE_TO_TASK("Обновление end_date задачи",
            "update task set end_date = :end_date " +
                    "where task_id=:task_id"),

    UPDATE_STATUS_TO_TASK("Обновление status_id задачи",
            "update task set status_id = :status_id " +
                    "where task_id=:task_id"),

    UPDATE_ASSIGNEE_ID_TO_TASK("Обновление assignee_id задачи",
            "update task set assignee_id = :assignee_id " +
                    "where task_id=:task_id");

    private final String description;
    private final String query;
}
