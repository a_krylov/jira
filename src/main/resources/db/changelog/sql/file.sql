CREATE TABLE file
(
    file_id serial PRIMARY KEY,
	task_id integer references task(task_id),
	name varchar(50) not null,
	path varchar(200) not null
);

grant select, insert on file to app;
grant usage on file_file_id_seq to app;
grant select, insert, update, delete on file to admin;