package ru.mephi.jira.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Workflow {
    private Status currentStatus;
    private List<Status> allowedStatuses;
}
