package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.Comment;
import ru.mephi.jira.model.File;
import ru.mephi.jira.model.ProjectUser;
import ru.mephi.jira.model.forController.CreateCommentRequest;
import ru.mephi.jira.model.forController.CreateFileRequest;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;

import static ru.mephi.jira.dao.query.FileQuery.*;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class FileDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Autowired
    public FileDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<File> fetchById(Integer taskId) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("task_id", taskId);
        return namedParameterJdbcTemplate.query(GET_FILE_BY_TASK_ID.getQuery(), params, (rs, i) -> {
            File file = File.builder()
                    .id(rs.getInt("file_id"))
                    .name(rs.getString("name"))
                    .path(rs.getString("path"))
                    .build();
            return file;
        });

    }

    public Integer insertFile(Integer taskId, CreateFileRequest createFileRequest) {
        Integer fileId = getSequenceNextValue();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("file_id", fileId)
                .addValue("task_id", taskId)
                .addValue("name", createFileRequest.getName())
                .addValue("path", createFileRequest.getPath());
        namedParameterJdbcTemplate.update(INSERT_FILE.getQuery(), params);
        return fileId;
    }

    private Integer getSequenceNextValue() {
        return namedParameterJdbcTemplate.queryForObject(GET_SEQUENCE_NEXT_VALUE.getQuery(), new HashMap<>(), Integer.class);
    }

}
