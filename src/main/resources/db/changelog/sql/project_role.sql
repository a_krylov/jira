CREATE TABLE project_role
(
    project_role_id serial PRIMARY KEY,
    project_role varchar(30) NOT NULL,
	description varchar(200)
);

grant select on project_role to app;
grant usage on project_role_project_role_id_seq to app;
grant select, insert, update, delete on project_role to admin;

insert into project_role values (nextval('project_role_project_role_id_seq'), 'owner', 'Владелец проекта');
insert into project_role values (nextval('project_role_project_role_id_seq'), 'dev', 'Разработчик');
insert into project_role values (nextval('project_role_project_role_id_seq'), 'architect', 'Архитектор');
insert into project_role values (nextval('project_role_project_role_id_seq'), 'systemAnalysis', 'Системный аналитик');
insert into project_role values (nextval('project_role_project_role_id_seq'), 'businessAnalysis', 'Бизнес аналитик');
insert into project_role values (nextval('project_role_project_role_id_seq'), 'QA', 'Тестировщик');
insert into project_role values (nextval('project_role_project_role_id_seq'), 'designer', 'Дизайнер');