package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TaskLinksQuery {
    GET_TASK_LINKS_BY_TASK_ID("Получение task_links по task_id",
            "select * from task_links " +
                    "where main_task_id=:main_task_id "),

    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса",
            "select * from nextval('task_links_id_seq')"),

    INSERT_TASK_LINK("Создание comment",
            "insert into task_links (" +
                    "id, main_task_id,  " +
                    "slave_task_id, link_type) " +
                    "   values (:id, :main_task_id, " +
                    ":slave_task_id, :link_type)");

    private final String description;
    private final String query;
}
