package ru.mephi.jira.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;
import ru.mephi.jira.dao.WorkflowDao;
import ru.mephi.jira.model.Status;
import ru.mephi.jira.model.Workflow;
import ru.mephi.jira.model.forController.WorkflowRaw;
import ru.mephi.jira.service.WorkflowService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WorkflowController {
    private final WorkflowDao workflowDao;
    private final WorkflowService workflowService;

    @GetMapping(value = "/workflow/getAllStatuses", produces = APPLICATION_JSON_VALUE)
    public List<Status> getAllStatuses() {
        return workflowDao.getAllStatuses();
    }

    @GetMapping(value = "/workflow/getAllowedStatusesForCurrentStatus", produces = APPLICATION_JSON_VALUE)
    public Workflow getAllowedStatuses(@RequestParam Integer currentStatusId, @RequestParam Integer taskTypeId) {
        return workflowService.getAllowedStatusesForCurrentStatus(currentStatusId, taskTypeId);
    }

    @GetMapping(value = "/workflow/getAllowedStatuses", produces = APPLICATION_JSON_VALUE)
    public List<Workflow> getAllowedStatuses(@RequestParam Integer taskTypeId) {
        return workflowService.getAllowedStatusesForTaskType(taskTypeId);
    }

    @PostMapping(value = "/workflow/createWorkflowRaw", consumes = APPLICATION_JSON_VALUE)
    public void createWorkflowRaw(@RequestBody WorkflowRaw workflowRaw) throws DataAccessException {
        workflowService.insertWorkflowRaw(workflowRaw);
    }

}
