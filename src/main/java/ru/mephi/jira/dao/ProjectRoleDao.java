package ru.mephi.jira.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import ru.mephi.jira.model.ProjectRole;
import ru.mephi.jira.model.SystemUser;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static ru.mephi.jira.dao.query.ProjectRoleQuery.GET_PROJECT_ROLES;
import static ru.mephi.jira.dao.query.ProjectRoleQuery.GET_PROJECT_ROLE_ID_BY_NAME;
import static ru.mephi.jira.infra.DataSourceConfig.JIRA;

@Slf4j
@Repository
public class ProjectRoleDao {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;


    @Autowired
    public ProjectRoleDao(@Qualifier(JIRA) DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    public List<ProjectRole> fetchAll() {
        return namedParameterJdbcTemplate.query(GET_PROJECT_ROLES.getQuery(), new ProjectRoleDao.ProjectRoleMapper());
    }

    public Integer fetchByName(String projectRole) {
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue("project_role", projectRole);
        return namedParameterJdbcTemplate.queryForObject(GET_PROJECT_ROLE_ID_BY_NAME.getQuery(), params, Integer.class);
    }

    public static class ProjectRoleMapper implements RowMapper<ProjectRole> {
        @Override
        public ProjectRole mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new ProjectRole(
                    rs.getInt("project_role_id"),
                    rs.getString("project_role"),
                    rs.getString("description")
            );
        }
    }

}
