package ru.mephi.jira.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class Project {
    private Integer id;
//    private List<Task> tasks;
    private String name;
    private String shortDescription;
    private String fullDescription;
    private LocalDateTime createDate;
    private SystemUser owner; //По спеке тут ProjectUser, но так гораздо лучше
//    private List<ProjectUser> users;

    public Project(String name, String shortDescription) {
        this.name = name;
        this.shortDescription = shortDescription;
    }

    public Project(Integer id) {
        this.id = id;
    }
}
