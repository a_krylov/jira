package ru.mephi.jira.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
@RequiredArgsConstructor
@AllArgsConstructor
public class Task {
    private Integer id;
    private Project project;
    private String name;
    private String shortDescription;
    private String fullDescription;
    private Status status;
    private TaskType taskType;
    private Integer priority;
    private ProjectUser author;
    private ProjectUser assignee;
    private LocalDateTime createDate;
    private LocalDate planDate;
    private LocalDate endDate;
    private List<Comment> comments;
    private List<File> files;
    private List<LinkedTask> linkedTasks;

    public Task(Integer id) {
        this.id = id;
    }
}
