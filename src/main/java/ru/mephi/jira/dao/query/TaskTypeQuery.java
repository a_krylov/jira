package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum TaskTypeQuery {
    GET_TASK_TYPES("Получение всех task_type",
            "select * from task_type order by task_type_id"),

    GET_TASK_TYPE_BY_ID("Получение task_type",
            "select * from task_type " +
                    "where task_type_id=:task_type_id"),

    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса",
            "select * from nextval('task_type_task_type_id_seq')"),

    INSERT_TASK_TYPE("Создание task_type",
            "insert into task_type (task_type_id, name) " +
                    "values (:task_type_id, :name)");

    private final String description;
    private final String query;
}
