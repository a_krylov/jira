package ru.mephi.jira.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class TaskType {
    private Integer id;
    private String name;

    public TaskType(String name) {
        this.name = name;
    }

    public TaskType(Integer id) {
        this.id = id;
    }
}
