CREATE TABLE task
(
    task_id serial PRIMARY KEY,
    project_id integer not null references project(project_id),
	task_name varchar(10) not null,
	short_description varchar(50) not null,
	full_description varchar(1000),
	status_id integer references status(status_id),
	task_type_id integer not null references task_type(task_type_id),
	author_id integer not null references project_user(project_user_id),
	assignee_id integer not null references project_user(project_user_id),
	create_date timestamp not null,
	plan_date timestamp,
	end_date timestamp,
    priority integer CHECK (priority > 0 and priority < 11)
);

ALTER TABLE task ADD CONSTRAINT constraint_task_name UNIQUE (task_name);
alter table task alter column assignee_id drop not null;
alter table task alter column plan_date type date;
alter table task alter column end_date type date;

grant select, update, insert on task to app;
grant usage on task_task_id_seq to app;
grant select, insert, update, delete on task to admin;