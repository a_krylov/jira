package ru.mephi.jira.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.web.bind.annotation.*;
import ru.mephi.jira.dao.CommentDao;
import ru.mephi.jira.dao.FileDao;
import ru.mephi.jira.dao.TaskDao;
import ru.mephi.jira.dao.TaskLinkDao;
import ru.mephi.jira.exeption.NotStatusException;
import ru.mephi.jira.model.Task;
import ru.mephi.jira.model.forController.*;
import ru.mephi.jira.service.TaskService;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class TaskController {
    private final TaskDao taskDao;
    private final CommentDao commentDao;
    private final FileDao fileDao;
    private final TaskLinkDao taskLinkDao;
    private final TaskService taskService;

    @GetMapping(value = "/task/getByTaskId/{taskId}", produces = APPLICATION_JSON_VALUE)
    public Task getByTaskId(@PathVariable("taskId") Integer taskId) {
        return taskService.getFullTask(taskId);
    }

    @GetMapping(value = "/task/getByProjectId", produces = APPLICATION_JSON_VALUE)
    public List<Task> getByProjectId(@RequestParam Integer projectId) {
        return taskService.getFullTasksByProjectId(projectId);
    }

    @PostMapping(value = "/task/create", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public Integer createTask(@RequestBody CreateTaskRequest createTaskRequest) throws DataAccessException {
        Integer taskId = taskDao.insertTask(createTaskRequest);
        return taskId;
    }

    @PostMapping(value = "/task/setFullDescription/{taskId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void setFullDescriptionToTask(@PathVariable("taskId") Integer taskId, @RequestBody SetFullDescriptionRequest setFullDescriptionRequest) throws DataAccessException {
        taskDao.updateFullDescription(taskId, setFullDescriptionRequest.getFullDescription());
    }

    @PostMapping(value = "/task/setPlanDate/{taskId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void setPlanDateToTask(@PathVariable("taskId") Integer taskId, @RequestBody SetDateRequest setDateRequest) throws DataAccessException {
        taskDao.updatePlanDate(taskId, setDateRequest.getDate());
    }

    @PostMapping(value = "/task/setEndDate/{taskId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void setEndDateToTask(@PathVariable("taskId") Integer taskId, @RequestBody SetDateRequest setDateRequest) throws DataAccessException {
        taskDao.updateEndDate(taskId, setDateRequest.getDate());
    }

    @PostMapping(value = "/task/setStatus/{taskId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void setStatusToTask(@PathVariable("taskId") Integer taskId, @RequestParam Integer statusId) throws NotStatusException {
        taskService.updateStatus(taskId, statusId);
    }

    @PostMapping(value = "/task/setAssignee/{taskId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void setAssigneeToTask(@PathVariable("taskId") Integer taskId, @RequestParam Integer projectUserId)  throws DataAccessException {
        taskService.updateAssignee(taskId, projectUserId);
    }

    @PostMapping(value = "/task/addComment/{taskId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void addCommentToTask(@PathVariable("taskId") Integer taskId, @RequestBody CreateCommentRequest createCommentRequest)  throws DataAccessException {
        commentDao.insertComment(taskId, createCommentRequest);
    }

    @PostMapping(value = "/task/addFile/{taskId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void addFileToTask(@PathVariable("taskId") Integer taskId, @RequestBody CreateFileRequest createFileRequest)  throws DataAccessException {
        fileDao.insertFile(taskId, createFileRequest);
    }

    @PostMapping(value = "/task/addTaskLink/{taskId}", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public void addTaskLink(@PathVariable("taskId") Integer taskId, @RequestBody CreateTaskLinkRequest createTaskLinkRequest)  throws DataAccessException {
        taskLinkDao.insertTaskLink(taskId, createTaskLinkRequest);
    }
}
