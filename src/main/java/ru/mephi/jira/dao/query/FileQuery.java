package ru.mephi.jira.dao.query;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum FileQuery {
    GET_FILE_BY_TASK_ID("Получение file по task_id",
            "select * from file " +
                    "where task_id=:task_id "),

    GET_SEQUENCE_NEXT_VALUE("Получение следующего значение сиквенса",
            "select * from nextval('file_file_id_seq')"),

    INSERT_FILE("Создание file",
            "insert into file (" +
                    "file_id, task_id, " +
                    "name, path) " +
                    "   values (:file_id, :task_id, " +
                    ":name, :path)");

    private final String description;
    private final String query;
}
