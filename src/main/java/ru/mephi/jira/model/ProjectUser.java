package ru.mephi.jira.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class ProjectUser {
    private Integer id;
    private Project project;
    private SystemUser systemUser;
    private List<ProjectRole> projectRoles;

    public ProjectUser(Integer id) {
        this.id = id;
    }


}
